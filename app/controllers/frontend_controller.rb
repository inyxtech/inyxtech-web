class FrontendController < ApplicationController
	layout 'layouts/frontend/application'
	
  def index
  	@catalog = KepplerCatalogs::Catalog.find_by_section("diseño web")
  	@attachments_web = @catalog ?  @catalog.attachments.where(public: true).limit(9) : Array.new 
  	@message = KepplerContactUs::Message.new
  end
end
