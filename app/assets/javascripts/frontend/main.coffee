removeHash = ->
  history.pushState '', document.title, window.location.pathname + window.location.search
  return

$(document).on 'ready page:load', () -> 
  #one-page-nav
  $('.top-nav').onePageNav
    currentClass: 'current'
    changeHash: false
    scrollSpeed: 500
    filter: ':not(.external)'
    begin: ->
      removeHash()
      return
    end: ->
      if $('.top-nav .current').find('a').attr('href') == '#about'
        $('.mobile-show').animate {
          opacity: 1
          left: '+=600'
        }, 300, ->
          $('#mobile-img').removeClass 'mobile-show'
          return
      if $('.top-nav .current').find('a').attr('href') != '#home'
        $('#front-navbar').removeClass 'background-hide'
        $('#front-navbar').addClass 'background-show'
      else
        $('#front-navbar').removeClass 'background-show'
        $('#front-navbar').addClass 'background-hide'
      return
    scrollChange: ($currentListItem) ->
      if $('.top-nav .current a').attr('href') == '#about'
        $('.mobile-show').animate {
          opacity: 1
          left: '+=600'
        }, 300, ->
          $('#mobile-img').removeClass 'mobile-show'
          return
      return

  #SyntaxHighlighter
  SyntaxHighlighter.all()
  return